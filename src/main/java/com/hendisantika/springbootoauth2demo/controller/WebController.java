package com.hendisantika.springbootoauth2demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-18
 * Time: 08:37
 */
@Controller
public class WebController {

    @RequestMapping({"/", "index"})
    public String init() {
        return "index";
    }

    @RequestMapping("/private")
    public String privatePage() {
        return "private";
    }

    @RequestMapping("/public")
    public String loginPub() {
        return "public";
    }

    @RequestMapping("/admin")
    public String admin() {
        return "admin";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }
}